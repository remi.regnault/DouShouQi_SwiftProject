//
//  GameError.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public enum GameError: Error {
    case invalidMove, nextPlayerError
}
