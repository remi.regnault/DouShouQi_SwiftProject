//
//  CellType.swift
//  
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation
import DouShouQI_Model

extension CellType: Codable {
    enum Key: CodingKey {
        case value
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let value = try container.decode(String.self, forKey: .value)
        switch value {
            case "unknown":
                self = .unknown
            case "jungle":
                self = .jungle
            case "water":
                self = .water
            case "trap":
                self = .trap
            case "den":
                self = .den
            default:
                throw LoadingError.unknownValue(value: value)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
            case .unknown:
                try container.encode("unknown", forKey: .value)
            case .jungle:
                try container.encode("jungle", forKey: .value)
            case .water:
                try container.encode("water", forKey: .value)
            case .trap:
                try container.encode("trap", forKey: .value)
            case .den:
                try container.encode("den", forKey: .value)
        }
    }
}
