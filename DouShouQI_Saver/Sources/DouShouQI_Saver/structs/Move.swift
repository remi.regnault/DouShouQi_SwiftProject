//
//  Move.swift
//
//
//  Created by etudiant on 2/12/24.
//

import Foundation
import DouShouQI_Model

extension Move: Codable {
    enum Key: CodingKey {
        case owner
        case rowOrigin
        case columnOrigin
        case rowDestination
        case columnDestination
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(self.owner, forKey: .owner)
        try container.encode(self.rowOrigin, forKey: .rowOrigin)
        try container.encode(self.columnOrigin, forKey: .columnOrigin)
        try container.encode(self.rowDestination, forKey: .rowDestination)
        try container.encode(self.columnDestination, forKey: .columnDestination)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let owner = try container.decode(Owner.self, forKey: .owner)
        let rowOrigin = try container.decode(Int.self, forKey: .rowOrigin)
        let columnOrigin = try container.decode(Int.self, forKey: .columnOrigin)
        let rowDestination = try container.decode(Int.self, forKey: .rowDestination)
        let columnDestination = try container.decode(Int.self, forKey: .columnDestination)
        self.init(owner: owner, rowOrigin: rowOrigin, columnOrigin: columnOrigin, rowDestination: rowDestination, columnDestination: columnDestination)
    }
}
