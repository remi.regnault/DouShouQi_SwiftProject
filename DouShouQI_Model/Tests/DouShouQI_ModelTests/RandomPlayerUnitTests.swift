//
//  RandomPlayerUnitTests.swift
//  
//
//  Created by Rémi REGNAULT on 31/01/2024.
//

import XCTest
@testable import DouShouQI_Model

final class RandomPlayerUnitTests: XCTestCase {
    
    private var board: Board?
    private var verySimpleRules: VerySimpleRules?
    private var randomPlayer: RandomPlayer?
    
    override func setUpWithError() throws {
        self.board = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])
        self.verySimpleRules = VerySimpleRules()
        self.randomPlayer = RandomPlayer(withName: "My name", andId: .player2)
    }

    func testChooseMove() throws {
        XCTAssertNotNil(randomPlayer?.chooseMove(in: board!, with: verySimpleRules!))
    }
    
}
