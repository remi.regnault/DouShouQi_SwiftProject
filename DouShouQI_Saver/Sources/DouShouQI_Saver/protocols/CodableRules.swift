//
//  Rules.swift
//  
//
//  Created by Rémi REGNAULT on 13/02/2024.
//

import Foundation
import DouShouQI_Model

public protocol CodableRules: Rules, Codable {
    func encode(to encoder: Encoder) throws
    init(from decoder: Decoder) throws
}
