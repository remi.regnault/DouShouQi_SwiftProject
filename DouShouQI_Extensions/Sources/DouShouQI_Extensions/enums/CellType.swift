//
//  CellType.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation
import DouShouQI_Model

extension CellType {
    public var symbol: String {
        switch self {
            case .unknown:
                return " "
            case .jungle:
                return "🌿"
            case .den:
                return "🪹"
            case .water:
                return "💧"
            case .trap:
                return "🪤"
        }
    }
}
