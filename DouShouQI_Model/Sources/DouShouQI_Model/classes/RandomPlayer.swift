//
//  RandomPlayer.swift
//  
//
//  Created by Rémi REGNAULT on 29/01/2024.
//

import Foundation

public class RandomPlayer: Player {
    public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
        let possible_moves = rules.getMoves(board: board, owner: self.id)
        let move_chosen_i = Int.random(in: 0..<possible_moves.count)
        let move_chosen = possible_moves[move_chosen_i]
        return move_chosen
    }
}
