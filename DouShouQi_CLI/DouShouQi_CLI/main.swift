//
//  main.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Extensions

func tpOneAndTwo() throws {
    try MainTpOneAndTwo.main()
}

func gameplayTwoRandomPlayer() throws {
    try MainGameplayTwoRandomPlayer.main()
}

func gameplayOneRandomAndOneHuman() throws {
    try MainGameplayOneHumanAndOneRandomPlayer.main()
}

func gameplayTwoHumanWithStructGame() throws {
    try MainGameplayWithStructGame.main()
}

func testsSaves() throws {
    try MainTestsSaves.main()
}

try testsSaves()
