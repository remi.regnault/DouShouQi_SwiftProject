//
//  WinningReason.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public enum WinningReason {
    case unknown, denReached, noMorePieces, tooManyOccurences, noMoveLeft
}
