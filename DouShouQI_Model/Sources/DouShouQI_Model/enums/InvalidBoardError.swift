//
//  InvalidBoardError.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public enum InvalidBoardError: Error {
    case badDimensions(rowNumber: Int, columnNumber: Int)
    case badCellType(cellType: CellType, row: Int, column: Int)
    case multipleOccurenceOfSamePiece(piece: Piece)
    case pieceWithNoOwner(piece: Piece)
    case pieceNotAllowedOnThisCell(piece: Piece, cell: Cell)
    case mulipleDen(owner: Owner)
}
