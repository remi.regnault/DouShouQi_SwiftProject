//
//  HumanPlayerTests.swift
//  
//
//  Created by Rémi REGNAULT on 31/01/2024.
//

import XCTest
@testable import DouShouQI_Model

final class HumanPlayerTests: XCTestCase {

    private var board: Board?
    private var verySimpleRules: VerySimpleRules?
    private var humanPlayer: HumanPlayer?
    private var inputValid: ((HumanPlayer) -> Move)?
    private var inputInvalid: ((HumanPlayer) -> Move)?
    
    override func setUpWithError() throws {
        self.board = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])
        self.verySimpleRules = VerySimpleRules()
        
        func inputValid(_ hm: HumanPlayer) -> Move {
            return Move(owner: hm.id, rowOrigin: 0, columnOrigin: 0, rowDestination: 1, columnDestination: 0)
        }
        self.inputValid = inputValid(_:)
        
        func inputInvalid(_ hm: HumanPlayer) -> Move {
            return Move(owner: hm.id, rowOrigin: 0, columnOrigin: 0, rowDestination: 0, columnDestination: 1)
        }
        self.inputInvalid = inputInvalid(_:)
    }

    func testInit() throws {
        XCTAssertNotNil(HumanPlayer(withName: "My name", andId: .player1, andInputMethod: inputValid!))
    }

    func testChooseMove() throws {
        XCTAssertNotNil(HumanPlayer(withName: "My name", andId: .player1, andInputMethod: self.inputValid!)?.chooseMove(in: self.board!, with: self.verySimpleRules!))
        XCTAssertNil(HumanPlayer(withName: "My name", andId: .player1, andInputMethod: self.inputInvalid!)?.chooseMove(in: self.board!, with: self.verySimpleRules!))
    }
}
