// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DouShouQI_Saver",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "DouShouQI_Saver",
            targets: ["DouShouQI_Saver"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "DouShouQI_Model", path: "../DouShouQI_Model"),
        .package(name: "DouShouQI_Extensions", path: "../DouShouQI_Extensions")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "DouShouQI_Saver",
            dependencies: ["DouShouQI_Model", "DouShouQI_Extensions"]),
        .testTarget(
            name: "DouShouQI_SaverTests",
            dependencies: ["DouShouQI_Saver"]),
    ]
)
