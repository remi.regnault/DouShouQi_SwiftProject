//
//  BoardResult.swift
//  
//
//  Created by Rémi REGNAULT on 16/01/2024.
//

import Foundation

public enum BoardResult: Equatable {
    case unknown, ok, failed(reason: BoardFailingReason)
}
