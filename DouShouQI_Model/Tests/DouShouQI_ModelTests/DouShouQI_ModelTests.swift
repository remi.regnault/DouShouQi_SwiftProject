import XCTest
@testable import DouShouQI_Model

final class DouShouQI_ModelTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DouShouQI_Model().text, "Hello, World!")
    }
}
