//
//  MainTestsSaves.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Saver

struct MainTestsSaves {
    static func main() throws {
        let piece: Piece = Piece(andAnimal: .cat, withOwner: .player1)
        let encodedPiece = try JSONEncoder().encode(piece)
        print(encodedPiece)
        let decodedPiece = try JSONDecoder().decode(Piece.self, from: encodedPiece)
        print(decodedPiece)
        
        let move: Move = Move(owner: .player1, rowOrigin: 1, columnOrigin: 1, rowDestination: 2, columnDestination: 2)
        let encodedMove = try JSONEncoder().encode(move)
        print(encodedMove)
        let decodedMove = try JSONDecoder().decode(Move.self, from: encodedMove)
        print(decodedMove)
        
        let cell: Cell = Cell(ofType: .den, ownedBy: .player1, withPiece: piece)
        let encodedCell = try JSONEncoder().encode(cell)
        print(encodedCell)
        let decodedCell = try JSONDecoder().decode(Cell.self, from: encodedCell)
        print(decodedCell)
        
        let cellNoOwner: Cell = Cell(ofType: .jungle, withPiece: piece)
        let encodedCellNoOwner = try JSONEncoder().encode(cellNoOwner)
        print(encodedCellNoOwner)
        let decodedCellNoOwner = try JSONDecoder().decode(Cell.self, from: encodedCellNoOwner)
        print(decodedCellNoOwner)
        
        let cellNoPiece: Cell = Cell(ofType: .den, ownedBy: .player1)
        let encodedCellNoPiece = try JSONEncoder().encode(cellNoPiece)
        print(encodedCellNoPiece)
        let decodedCellNoPiece = try JSONDecoder().decode(Cell.self, from: encodedCellNoPiece)
        print(decodedCellNoPiece)
        
        let board = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])
        let encodedBoard = try JSONEncoder().encode(board)
        print(encodedBoard)
        let decodedBoard = try JSONDecoder().decode(Board.self, from: encodedBoard)
        print(decodedBoard)
        
        let boardBefore = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])
        let boardAfter = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle)
            ]
        ])
        let moveForRules = Move(owner: .player1, rowOrigin: 2, columnOrigin: 0, rowDestination: 2, columnDestination: 1)
        var rules: CodableRules = VerySimpleRules()
        rules.playedMove(move: moveForRules, boardBefore: boardBefore!, boardAfter: boardAfter!)
        let encodedRules = try JSONEncoder().encode(rules)
        print(encodedRules)
        let decodedRules = try JSONDecoder().decode(VerySimpleRules.self, from: encodedRules)
        print(decodedRules)
    }
}
