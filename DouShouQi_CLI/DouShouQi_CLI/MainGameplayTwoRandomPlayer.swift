//
//  MainGameplayTwoRandomPlayer.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 05/02/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Extensions

struct MainGameplayTwoRandomPlayer {
    static func main() throws {
        func playMove(board: Board, move: Move) -> Board {
            var boardOut = board
            print(board)
            print(move)
            if board.grid[move.rowDestination][move.columnDestination].piece != nil {
                _ = boardOut.removePiece(atRow: move.rowDestination, andColumn: move.columnDestination)
            }
            _ = boardOut.insert(piece: board.grid[move.rowOrigin][move.columnOrigin].piece!, atRow: move.rowDestination, andColumn: move.columnDestination)
            _ = boardOut.removePiece(atRow: move.rowOrigin, andColumn: move.columnOrigin)
            return boardOut
        }

        let randomPlayerOne = RandomPlayer(withName: "Random Player 1", andId: .player1)
        let randomPlayerTwo = RandomPlayer(withName: "Random Player 2", andId: .player2)

        var rules = VerySimpleRules()
        var board_before = VerySimpleRules.createBoard()

        var move_chosen = randomPlayerOne!.chooseMove(in: board_before, with: rules)

        var board_after = playMove(board: board_before, move: move_chosen!)
        rules.playedMove(move: move_chosen!, boardBefore: board_before, boardAfter: board_after)

        while (!rules.isGameOver(board: board_after, lastCellRow: move_chosen!.rowDestination, lastCellColumn: move_chosen!.columnDestination).0) {
            let next_player_id = rules.getNextPlayer()
            var next_player: Player?
            switch next_player_id {
                case .player1:
                    next_player = randomPlayerOne
                    break
                case .player2:
                    next_player = randomPlayerTwo
                    break
                case .noOne:
                    print("an error occured")
            }
            board_before = board_after
            move_chosen = next_player?.chooseMove(in: board_before, with: rules)
            board_after = playMove(board: board_before, move: move_chosen!)
            rules.playedMove(move: move_chosen!, boardBefore: board_before, boardAfter: board_after)
        }
        print("The game is over!")
    }
}

