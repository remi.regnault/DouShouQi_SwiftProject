//
//  Piece.swift
//  
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation
import DouShouQI_Model

extension Piece: Codable {
    enum Key: CodingKey {
        case owner
        case animal
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(self.owner, forKey: .owner)
        try container.encode(self.animal, forKey: .animal)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let owner = try container.decode(Owner.self, forKey: .owner)
        let animal = try container.decode(Animal.self, forKey: .animal)
        self.init(andAnimal: animal, withOwner: owner)
    }
}
