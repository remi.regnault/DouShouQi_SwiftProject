//
//  RandomPlayer.swift
//  
//
//  Created by Rémi REGNAULT on 31/01/2024.
//

import XCTest
@testable import DouShouQI_Model

final class PlayerUnitTests: XCTestCase {

    private var board: Board?
    private var verySimpleRules: VerySimpleRules?
    
    override func setUpWithError() throws {
        self.board = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])
        self.verySimpleRules = VerySimpleRules()
    }
    
    func testInit() throws {
        XCTAssertNil(Player(withName: "oui", andId: .noOne))
        XCTAssertNil(Player(withName: "", andId: .player1))
        XCTAssertNotNil(Player(withName: "Etienne", andId: .player1))
    }
    
    func testChooseMove() throws {
        XCTAssertNil(Player(withName: "Etienne", andId: .player1)?.chooseMove(in: self.board!, with: self.verySimpleRules!))
    }
}
