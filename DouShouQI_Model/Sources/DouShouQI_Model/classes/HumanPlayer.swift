//
//  HumanPlayer.swift
//  
//
//  Created by Rémi REGNAULT on 29/01/2024.
//

import Foundation

public class HumanPlayer: Player {
    public private(set) var inputMethod: ((HumanPlayer) -> Move)?
    
    public override init?(withName name: String, andId id: Owner) {
        super.init(withName: name, andId: id)
    }
    
    convenience public init?(withName name: String, andId id: Owner, andInputMethod inputMethod: @escaping (HumanPlayer) -> Move) {
        self.init(withName: name, andId: id)
        self.inputMethod = inputMethod
    }
    
    public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
        guard inputMethod != nil else {
            return nil
        }
        let move_chosen = inputMethod!(self)
        guard rules.isMoveValid(board: board, move: move_chosen) else {
            return nil
        }
        return move_chosen
    }
}
