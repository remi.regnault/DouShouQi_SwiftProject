//
//  Board.swift
//
//
//  Created by etudiant on 2/12/24.
//

import Foundation
import DouShouQI_Model

extension Board: Codable {
    enum Key: CodingKey {
        case grid
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(self.grid, forKey: .grid)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let grid = try container.decode([[Cell]].self, forKey: .grid)
        self.init(withGrid: grid)!
    }
}
