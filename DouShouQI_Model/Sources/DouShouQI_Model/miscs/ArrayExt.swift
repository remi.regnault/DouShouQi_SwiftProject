//
//  File.swift
//  
//
//  Created by Rémi REGNAULT on 17/01/2024.
//

import Foundation

public extension Array where Element == [Cell] {
    func countForPlayer(owner: Owner) -> Int {
        var counter = 0
        for row in self {
            for cell in row {
                guard let currentPiece: Piece = cell.piece else {
                    continue
                }
                if (currentPiece.owner == owner) {
                    counter += 1
                }
            }
        }
        return counter
    }
}
