//
//  MainGameplayWithStructGame.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 05/02/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Extensions

enum Gamemode: Equatable {
    case TwoAIs, OneAIOneHuman, TwoPlayers
}

struct MainGameplayWithStructGame {
    static func main() throws {
        func chooseGamemode() -> Gamemode {
            print("Please choose mode :")
            print("\t 1 - Two Random Players mode")
            print("\t 2 - One Random Player and One Human")
            print("\t 3 - Two Human Players")
            print("Your choice :")
            var choice = readLine()
            while (choice == nil || Int(choice!) == nil || Int(choice!)! > 3 || Int(choice!)! <= 0 ) {
                print("Invalid value. Please, choose again :")
                choice = readLine()
            }
            let finalChoice = Int(choice!)!
            switch (finalChoice) {
                case 1:
                    return .TwoAIs
                case 2:
                    return .OneAIOneHuman
                case 3:
                    return .TwoPlayers
                default:
                    return .TwoPlayers
            }
        }
        func input(_ hm: HumanPlayer) -> Move {
            print("Choose row origin :")
            var rowOriginStr = readLine()
            while (rowOriginStr == nil || Int(rowOriginStr!) == nil) {
                print("Invalid value. Choose row origin :")
                rowOriginStr = readLine()
            }
            let rowOrigin = Int(rowOriginStr!)!
            print("Choose column origin :")
            var columnOriginStr = readLine()
            while (columnOriginStr == nil || Int(columnOriginStr!) == nil) {
                print("Invalid value. Choose column origin :")
                columnOriginStr = readLine()
            }
            let columnOrigin = Int(columnOriginStr!)!
            print("Choose row destination :")
            var rowDestinationStr = readLine()
            while (rowDestinationStr == nil || Int(rowDestinationStr!) == nil) {
                print("Invalid value. Choose row destination :")
                rowDestinationStr = readLine()
            }
            let rowDestination = Int(rowDestinationStr!)!
            print("Choose column destination :")
            var columnDestinationStr = readLine()
            while (columnDestinationStr == nil || Int(columnDestinationStr!) == nil) {
                print("Invalid value. Choose row destination :")
                columnDestinationStr = readLine()
            }
            let columnDestination = Int(columnDestinationStr!)!
            return Move(owner: hm.id, rowOrigin: rowOrigin, columnOrigin: columnOrigin, rowDestination: rowDestination, columnDestination: columnDestination)
        }
        func createPlayers(gamemode: Gamemode, input: @escaping (HumanPlayer)-> Move) -> (Player, Player) {
            print("Please enter name for player 1 :")
            let playerOneName = readLine()
            print("Please enter name for player 2 :")
            let playerTwoName = readLine()
            let defaultName = "no name"
            switch (gamemode) {
                case .TwoAIs:
                    return (RandomPlayer(withName: playerOneName ?? defaultName, andId: .player1)!, RandomPlayer(withName: playerTwoName ?? defaultName, andId: .player2)!)
                case .OneAIOneHuman:
                    return (HumanPlayer(withName: playerOneName ?? defaultName, andId: .player1, andInputMethod: input)!, RandomPlayer(withName: playerTwoName ?? defaultName, andId: .player2)!)
                case .TwoPlayers:
                    return (HumanPlayer(withName: playerOneName ?? defaultName, andId: .player1, andInputMethod: input)!, HumanPlayer(withName: playerTwoName ?? defaultName, andId: .player2, andInputMethod: input)!)
            }
        }
        
        let (playerOne, playerTwo) = createPlayers(gamemode: chooseGamemode(), input: input(_:))
        let rules: Rules = VerySimpleRules()
        let board: Board = VerySimpleRules.createBoard()
        var game: Game = Game(withRules: rules, andBoard: board, andPlayerOne: playerOne, andPlayerTwo: playerTwo)
        
        func onStart(_ board: Board) {
            print(board)
            print("**************************************")
            print("        ==>> GAME STARTS! <<==")
            print("**************************************")
        }
        game.setOnStart(onStartNotificationMethod: onStart(_:))
        
        func showBoardBeforePlayerTurn(_ board: Board, _ player: Player) {
            print(board)
            print("**************************************")
            print("Player \(player.id) - \(player.name), it's your turn!")
            print("**************************************")
        }
        game.setOnPlayerTurn(notifyPlayerTurnMethod: showBoardBeforePlayerTurn(_:_:))
        
        func onIsGameOver(_ result: Result) {
            switch (result) {
                case .notFinished:
                    print("Game is not over yet!")
                    return
                case .even:
                    return
                case let .winner(owner, winningReason):
                    print("**************************************")
                    print("Game Over!!!")
                    print("and the winner is... \(owner)!")
                    switch (winningReason) {
                        case .denReached:
                            print("the opponent's den has been reached.")
                            break
                        case .noMorePieces:
                            print("the opponent has no more pieces.")
                            break
                        case .noMoveLeft:
                            print("the opponent has no move possible.")
                            break
                        case .unknown:
                            print("the reason is unknown.")
                            break
                        case .tooManyOccurences:
                            print("there has been too many occurences.")
                            break
                    }
                    print("**************************************")
            }
        }
        game.setOnGameIsOver(onGameIsOverNotificationMethod: onIsGameOver(_:))
        
        func onPlayingMove(_ move: Move) {
            print("Move playing : \(move)")
        }
        game.setOnPlayingMove(notifyOnPlayingMoveMethod: onPlayingMove(_:))
        
        func onInvalidMove(_ player: Player, _ move: Move?) {
            print("Invalid move chosen : \(move)")
            print("Please \(player.name) (\(player.id)), choose again.")
        }
        game.setOnInvalidPlayerMove(notifyInvalidPlayerMoveMethod: onInvalidMove(_:_:))
        
        func onBoardChanged(_ board: Board) {
            print(board)
        }
        game.setOnBoardChanged(onBoardChangedNotificationMethod: onBoardChanged(_:))
        
        try game.start()
    }
    
    
}
