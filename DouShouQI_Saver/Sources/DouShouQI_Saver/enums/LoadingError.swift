//
//  LoadingError.swift
//  
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation

enum LoadingError: Error {
    case unknownValue(value: String)
}
