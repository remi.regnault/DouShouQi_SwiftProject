//
//  Result.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public enum Result: Equatable {
   case notFinished, even, winner(owner: Owner, winningReason: WinningReason)
}
