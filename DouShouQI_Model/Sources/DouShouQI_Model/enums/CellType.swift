//
//  CellType.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public enum CellType {
    case unknown, jungle, water, trap, den
}
