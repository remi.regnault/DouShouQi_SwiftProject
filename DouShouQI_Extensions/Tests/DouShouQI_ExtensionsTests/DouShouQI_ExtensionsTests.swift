import XCTest
@testable import DouShouQI_Extensions

final class DouShouQI_ExtensionsTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DouShouQI_Extensions().text, "Hello, World!")
    }
}
