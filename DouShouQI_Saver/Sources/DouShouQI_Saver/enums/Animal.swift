//
//  Animal.swift
//  
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation
import DouShouQI_Model

extension Animal: Codable {
    
    enum Key: CodingKey {
        case value
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let value = try container.decode(String.self, forKey: .value)
        switch value {
            case "rat":
                self = .rat
            case "cat":
                self = .cat
            case "dog":
                self = .dog
            case "wolf":
                self = .wolf
            case "leopard":
                self = .leopard
            case "tiger":
                self = .tiger
            case "lion":
                self = .lion
            case "elephant":
                self = .elephant
            default:
                throw LoadingError.unknownValue(value: value)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
            case .rat:
                try container.encode("rat", forKey: .value)
            case .cat:
                try container.encode("cat", forKey: .value)
            case .dog:
                try container.encode("dog", forKey: .value)
            case .wolf:
                try container.encode("wolf", forKey: .value)
            case .leopard:
                try container.encode("leopard", forKey: .value)
            case .tiger:
                try container.encode("tiger", forKey: .value)
            case .lion:
                try container.encode("lion", forKey: .value)
            case .elephant:
                try container.encode("elephant", forKey: .value)
        }
    }
}
