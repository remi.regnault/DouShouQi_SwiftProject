//
//  Owner.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public enum Owner : String, CustomStringConvertible {
    case noOne
    case player1
    case player2
    
    public var description: String {
        switch self {
            case .noOne:
                return "x"
            case .player1:
                return "1"
            case .player2:
                return "2"
        }
    }
}
