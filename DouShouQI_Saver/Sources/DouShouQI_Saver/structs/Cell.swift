//
//  Cell.swift
//
//
//  Created by etudiant on 2/12/24.
//

import Foundation
import DouShouQI_Model

extension Cell: Codable {
    enum Key: CodingKey {
        case initialOwner
        case cellType
        case piece
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(self.initialOwner, forKey: .initialOwner)
        try container.encode(self.cellType, forKey: .cellType)
        try container.encode(self.piece, forKey: .piece)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let initialOwner = try container.decode(Owner.self, forKey: .initialOwner)
        let cellType = try container.decode(CellType.self, forKey: .cellType)
        var piece: Piece?
        do {
            piece = try container.decode(Piece.self, forKey: .piece)
        } catch {}
        self.init(ofType: cellType, ownedBy: initialOwner, withPiece: piece)
    }
}
