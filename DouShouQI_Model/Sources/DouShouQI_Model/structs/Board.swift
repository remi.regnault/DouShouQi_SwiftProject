//
//  Board.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public struct Board: Equatable, Hashable {
    public let nbRows : Int
    public let nbColumns : Int
    public private(set) var grid : [[Cell]]

    public init?(withGrid grid : [[Cell]]) {
        guard !grid.isEmpty, !grid[0].isEmpty, grid.allSatisfy({ grid[0].count == $0.count }) else {
            return nil
        }
        self.grid = grid
        self.nbRows = grid.count
        self.nbColumns = grid[0].count
    }
    
    public static func == (lhs: Board, rhs: Board) -> Bool {
        return lhs.nbRows == rhs.nbRows && lhs.nbColumns == rhs.nbColumns && lhs.grid == rhs.grid
    }
    
    public func countPieces(of player : Owner) -> Int {
        return grid.countForPlayer(owner: player)
    }

    public func countPieces() -> (Int, Int) {
        return (pieceCountPlayer1: grid.countForPlayer(owner: .player1), pieceCountPlayer2: grid.countForPlayer(owner: .player2))
    }

    public mutating func insert(piece: Piece, atRow rowId : Int, andColumn columnId : Int) -> BoardResult {
        guard rowId < nbRows, rowId >= 0, columnId < nbColumns, columnId >= 0 else {
           return .failed(reason: .outOfBounds)
        }
        guard grid[rowId][columnId].piece == nil else {
            return .failed(reason: .cellNotEmpty)
        }
        grid[rowId][columnId].piece = piece
        return .ok
    }

    public mutating func removePiece(atRow rowId : Int, andColumn columnId : Int) -> BoardResult {
        guard rowId < nbRows, rowId >= 0, columnId < nbColumns, columnId >= 0 else {
            return .failed(reason: .outOfBounds)
        }
        guard grid[rowId][columnId].piece != nil else { 
            return .failed(reason: .cellEmpty)
        }
        grid[rowId][columnId].piece = nil
        return .ok
    }
}
