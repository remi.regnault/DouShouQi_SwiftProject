//
//  VerySimpleRules.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public struct VerySimpleRules : Rules {
    public private(set) var occurences: [Board: Int] = [:]
    public private(set) var historic: [Move] = []
    
    internal static let NB_ROWS = 5
    internal static let NB_COLUMNS = 5
    
    public init(withHistoric historic: [Move] = []) {
        self.historic = historic
    }
    
    public static func createBoard() -> Board {
        let grid: [[Cell]] = [
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .den, ownedBy: .player1),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player1)),
                Cell(ofType: .jungle)
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .den, ownedBy: .player2),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player2)),
                Cell(ofType: .jungle)
            ]
        ]
        return Board(withGrid: grid)!
    }

    public static func checkBoard(board: Board) throws {
        let grid = board.grid
        var denP1 = false
        var denP2 = false

        guard !grid.isEmpty, !grid[0].isEmpty, grid.allSatisfy({ self.NB_COLUMNS == $0.count }), grid.count == self.NB_ROWS else {
            throw InvalidBoardError.badDimensions(rowNumber: grid.count, columnNumber: grid[0].count)
        }

        var knownPieces: [Piece] = []
        for row_id in 0..<grid.count {
            for column_id in 0..<grid[row_id].count {
                let cell = grid[row_id][column_id]
                guard cell.cellType == .jungle || cell.cellType == .den else {
                    throw InvalidBoardError.badCellType(cellType: cell.cellType, row: row_id, column: column_id)
                }
                
                if let currentPiece: Piece = cell.piece {
                    guard cell.cellType != .den else {
                        throw InvalidBoardError.pieceNotAllowedOnThisCell(piece: currentPiece, cell: cell)
                    }
                    guard currentPiece.owner != .noOne else {
                        throw InvalidBoardError.pieceWithNoOwner(piece: currentPiece)
                    }
                    guard !knownPieces.contains(currentPiece) else {
                        throw InvalidBoardError.multipleOccurenceOfSamePiece(piece: currentPiece)
                    }
                    if (cell.cellType == .den) {
                        if (cell.initialOwner == .player1) {
                            guard !denP1 else {
                                throw InvalidBoardError.mulipleDen(owner: .player1)
                            }
                            denP1 = true
                        } else {
                            guard !denP2 else {
                                throw InvalidBoardError.mulipleDen(owner: .player2)
                            }
                            denP2 = true
                        }
                    }
                    knownPieces.append(currentPiece)
                }
            }
        }
    }

    public func getNextPlayer() -> Owner {
        return self.historic.last?.owner == .player1 ? .player2 : .player1
    }

    public func getMoves(board: Board, owner: Owner) -> [Move] {
        var moves: [Move] = []
        for row_id in 0..<board.nbRows {
            for column_id in 0..<board.nbColumns {
                guard let currentPiece: Piece = board.grid[row_id][column_id].piece else {
                    continue
                }
                if currentPiece.owner == owner {
                    moves.append(contentsOf: getMoves(board: board, owner: owner, row: row_id, column: column_id))
                }
            }
        }
        return moves
    }
    
    public func getMoves(board: Board, owner: Owner, row: Int, column: Int) -> [Move] {
        var moves: [Move] = []
        for row_modifier in [-1, 0, 1] {
            for column_modifier in [-1, 0, 1] {
                guard !(abs(row_modifier) == abs(column_modifier)) else {
                    continue
                }
                let move = Move(owner: owner, rowOrigin: row, columnOrigin: column, rowDestination: row + row_modifier, columnDestination: column + column_modifier)
                if isMoveValid(board: board, move: move) {
                    moves.append(move)
                }
            }
        }
        return moves
    }

    public func isMoveValid(board: Board, rowOrigin: Int, columnOrigin: Int, rowDestination: Int, columnDestination: Int) -> Bool {
        guard rowOrigin < board.nbRows, rowOrigin >= 0, columnOrigin < board.nbColumns, columnOrigin >= 0 else {
            return false
        }
        guard rowDestination < board.nbRows, rowDestination >= 0, columnDestination < board.nbColumns, columnDestination >= 0 else {
            return false
        }
        guard let pieceOnOrigin: Piece = board.grid[rowOrigin][columnOrigin].piece else {
            return false
        }
        if (board.grid[rowDestination][columnDestination].cellType == .den) {
            guard board.grid[rowDestination][columnDestination].initialOwner != pieceOnOrigin.owner else {
                return false
            }
        }

        if let pieceOnDestination: Piece = board.grid[rowDestination][columnDestination].piece {
            guard pieceOnDestination.owner != pieceOnOrigin.owner else {
                return false
            }
            guard (pieceOnOrigin.animal > pieceOnDestination.animal || (pieceOnOrigin.animal == .rat && pieceOnDestination.animal == .elephant)), !(pieceOnDestination.animal == .rat && pieceOnOrigin.animal == .elephant) else {
                return false
            }
        }
          
        return true
    }

    public func isMoveValid(board: Board, move: Move) -> Bool {
        return isMoveValid(board: board, rowOrigin: move.rowOrigin, columnOrigin: move.columnOrigin, rowDestination: move.rowDestination, columnDestination: move.columnDestination)
    }

    public func isGameOver(board: Board, lastCellRow: Int, lastCellColumn: Int) -> (Bool, Result) {
        guard board.countPieces(of: self.getNextPlayer()) > 0 else {
            return (true, .winner(owner: board.grid[lastCellRow][lastCellColumn].piece!.owner, winningReason: .noMorePieces))
        }
        guard getMoves(board: board, owner: self.getNextPlayer()).count > 0 else {
            return (true, .winner(owner: board.grid[lastCellRow][lastCellColumn].piece!.owner, winningReason: .noMoveLeft))
        }
        guard board.grid[lastCellRow][lastCellColumn].cellType != .den else {
            return (true, .winner(owner: board.grid[lastCellRow][lastCellColumn].piece!.owner, winningReason: .denReached))
        }
        return (false, .notFinished)
    }

    public mutating func playedMove(move: Move, boardBefore: Board, boardAfter: Board) {
        historic.append(move)
    }
}
