//
//  VerySimpleRulesTests.swift
//  
//
//  Created by Rémi REGNAULT on 24/01/2024.
//

import XCTest
@testable import DouShouQI_Model

final class VerySimpleRulesTests: XCTestCase {

    private var initialBoard: Board!
    private var rules: VerySimpleRules!
      
    override func setUpWithError() throws {
        self.initialBoard = Board(withGrid: [
                                      [
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                                          Cell(ofType: .den, ownedBy: .player1),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player1)),
                                          Cell(ofType: .jungle)
                                      ],
                                      [
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1)),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1))
                                      ],
                                      [
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle)
                                      ],
                                      [
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2)),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player2)),
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player2))
                                      ],
                                      [
                                          Cell(ofType: .jungle),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                                          Cell(ofType: .den, ownedBy: .player2),
                                          Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player2)),
                                          Cell(ofType: .jungle)
                                      ]
                                  ]
        )
        self.rules = VerySimpleRules()
    }
      
    func testCreateBoard() {
        XCTAssertEqual(self.initialBoard, VerySimpleRules.createBoard())
    }

    func testCheckBoard() {
        XCTAssertNoThrow(try VerySimpleRules.checkBoard(board: self.initialBoard))
        _ = initialBoard.insert(piece: Piece(andAnimal: .lion, withOwner: .player1), atRow: 0, andColumn: 0)
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(board: initialBoard))
        _ = initialBoard.removePiece(atRow: 0, andColumn: 0)
        _ = initialBoard.insert(piece: Piece(andAnimal: .lion, withOwner: .noOne), atRow: 0, andColumn: 0)
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(board: initialBoard))
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(board: Board(withGrid: [[Cell(ofType: .jungle)], [Cell(ofType: .jungle)]])!))
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(board: Board(withGrid: [
                                                                             [
                                                                                 Cell(ofType: .water),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .den),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle)
                                                                             ],
                                                                             [
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle)
                                                                             ],
                                                                             [
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle)
                                                                             ],
                                                                             [
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle)
                                                                             ],
                                                                             [
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .den),
                                                                                 Cell(ofType: .jungle),
                                                                                 Cell(ofType: .jungle)
                                                                             ]
                                                                         ])!))
    }

    func testGetMoves() {
        let board = Board(withGrid: [
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .rat, withOwner: .player1))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player2))
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .elephant, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ]
        ])!
        let expectedMovesP1 = [
            Move(owner: .player1, rowOrigin: 0, columnOrigin: 0, rowDestination: 1, columnDestination: 0),
            Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 1, columnDestination: 1),
            Move(owner: .player1, rowOrigin: 0, columnOrigin: 2, rowDestination: 1, columnDestination: 2),
            Move(owner: .player1, rowOrigin: 2, columnOrigin: 0, rowDestination: 1, columnDestination: 0),
            Move(owner: .player1, rowOrigin: 2, columnOrigin: 0, rowDestination: 2, columnDestination: 1),
        ]
        let expectedMovesP2 = [
            Move(owner: .player2, rowOrigin: 1, columnOrigin: 0, rowDestination: 1, columnDestination: 1),
            Move(owner: .player2, rowOrigin: 1, columnOrigin: 2, rowDestination: 1, columnDestination: 1),
            Move(owner: .player2, rowOrigin: 1, columnOrigin: 2, rowDestination: 2, columnDestination: 2),
        ]
        XCTAssertEqual(expectedMovesP1.count, rules.getMoves(board: board, owner: .player1).count)
        XCTAssertEqual(expectedMovesP2.count, rules.getMoves(board: board, owner: .player2).count)
    }

    func testPlayedMove() {
        XCTAssertEqual(0, self.rules.historic.count)
        let move = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 0, columnDestination: 0)
        self.rules.playedMove(move: move, boardBefore: self.initialBoard, boardAfter: self.initialBoard)
        XCTAssertEqual(1, self.rules.historic.count)
    }
    
    func testGetNextPlayer() {
        XCTAssertEqual(Owner.player1, rules.getNextPlayer())
        var boardAfter = initialBoard
        _ = boardAfter?.insert(piece: Piece(andAnimal: .tiger, withOwner: .player1), atRow: 0, andColumn: 0)
        _ = boardAfter?.removePiece(atRow: 0, andColumn: 1)
        rules.playedMove(move: Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 0, columnDestination: 0), boardBefore: self.initialBoard, boardAfter: boardAfter!)
        XCTAssertEqual(Owner.player2, rules.getNextPlayer())
    }
    
    func testIsGameOver() {
        XCTAssertEqual(false, rules.isGameOver(board: initialBoard, lastCellRow: 0, lastCellColumn: 1).0)
        XCTAssertEqual(Result.notFinished, rules.isGameOver(board: initialBoard, lastCellRow: 0, lastCellColumn: 1).1)
        let localBoard = Board(withGrid: [
            [
                Cell(ofType: .jungle),
                Cell(ofType: .den, ownedBy: .player1, withPiece: Piece(andAnimal: .elephant, withOwner: .player2)),
                Cell(ofType: .jungle),
            ],
            [
                Cell(ofType: .jungle, withPiece: Piece(andAnimal: .tiger, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle)
            ],
            [
                Cell(ofType: .jungle),
                Cell(ofType: .den, ownedBy: .player2),
                Cell(ofType: .jungle)
            ]
        ])
        XCTAssertEqual(true, rules.isGameOver(board: localBoard!, lastCellRow: 0, lastCellColumn: 1).0)
        XCTAssertEqual(Result.winner(owner: .player2, winningReason: .denReached), rules.isGameOver(board: localBoard!, lastCellRow: 0, lastCellColumn: 1).1)
//        var localBoardAfter = localBoard
//        _ = localBoardAfter?.removePiece(atRow: 0, andColumn: 1)
//        rules.playedMove(move: Move(owner: .player1, rowOrigin: 0, columnOrigin: 0, rowDestination: 0, columnDestination: 1), boardBefore: localBoard!, boardAfter: localBoardAfter!)
//        XCTAssertEqual(true, rules.isGameOver(board: localBoard!, lastCellRow: 1, lastCellColumn: 0).0)
//        XCTAssertEqual(Result.winner(owner: .player2, winningReason: .noMorePieces), rules.isGameOver(board: localBoard!, lastCellRow: 1, lastCellColumn: 0).1)
    }
}
