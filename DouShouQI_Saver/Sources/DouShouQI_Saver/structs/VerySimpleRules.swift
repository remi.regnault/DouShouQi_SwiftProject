//
//  File.swift
//  
//
//  Created by Rémi REGNAULT on 13/02/2024.
//

import Foundation
import DouShouQI_Model

extension VerySimpleRules: CodableRules {
    enum Key: CodingKey {
        case occurences
        case historic
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(self.occurences, forKey: .occurences)
        try container.encode(self.historic, forKey: .historic)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let historic = try container.decode([Move].self, forKey: .historic)
        self.init(withHistoric: historic)
    }
}
