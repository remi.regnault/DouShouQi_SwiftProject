//
//  Animal.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation
import DouShouQI_Model

extension Animal {
    public var symbol: String {
        switch self {
            case .rat:
                return "🐭"
            case .cat:
                return "🐱"
            case .dog:
                return "🐶"
            case .wolf:
                return "🐺"
            case .leopard:
                return "🐆"
            case .tiger:
                return "🐯"
            case .lion:
                return "🦁"
            case .elephant:
                return "🐘"
        }
    }
}
