//
//  Piece.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public struct Piece : CustomStringConvertible, Equatable, Hashable {
    public let owner : Owner
    public let animal : Animal
    
    public init(andAnimal animal: Animal, withOwner owner: Owner) {
        self.animal = animal
        self.owner = owner
    }
    
    public var description : String { "[\(owner):\(animal)]" }
}
