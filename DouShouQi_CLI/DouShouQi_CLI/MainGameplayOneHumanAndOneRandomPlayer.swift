//
//  MainGameplayOneHumanAndOneRandomPlayer.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 05/02/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Extensions

struct MainGameplayOneHumanAndOneRandomPlayer {
    static func main() throws {
        func playMove(board: Board, move: Move) -> Board {
            var boardOut = board
            print(board)
            print(move)
            if board.grid[move.rowDestination][move.columnDestination].piece != nil {
                _ = boardOut.removePiece(atRow: move.rowDestination, andColumn: move.columnDestination)
            }
            _ = boardOut.insert(piece: board.grid[move.rowOrigin][move.columnOrigin].piece!, atRow: move.rowDestination, andColumn: move.columnDestination)
            _ = boardOut.removePiece(atRow: move.rowOrigin, andColumn: move.columnOrigin)
            print(boardOut)
            return boardOut
        }
        
        func input(_ hm: HumanPlayer) -> Move {
            print("Choose row origin :")
            var rowOriginStr = readLine()
            while (rowOriginStr == nil || Int(rowOriginStr!) == nil) {
                print("Invalid value. Choose row origin :")
                rowOriginStr = readLine()
            }
            let rowOrigin = Int(rowOriginStr!)!
            print("Choose column origin :")
            var columnOriginStr = readLine()
            while (columnOriginStr == nil || Int(columnOriginStr!) == nil) {
                print("Invalid value. Choose column origin :")
                columnOriginStr = readLine()
            }
            let columnOrigin = Int(columnOriginStr!)!
            print("Choose row destination :")
            var rowDestinationStr = readLine()
            while (rowDestinationStr == nil || Int(rowDestinationStr!) == nil) {
                print("Invalid value. Choose row destination :")
                rowDestinationStr = readLine()
            }
            let rowDestination = Int(rowDestinationStr!)!
            print("Choose column destination :")
            var columnDestinationStr = readLine()
            while (columnDestinationStr == nil || Int(columnDestinationStr!) == nil) {
                print("Invalid value. Choose row destination :")
                columnDestinationStr = readLine()
            }
            let columnDestination = Int(columnDestinationStr!)!
            return Move(owner: hm.id, rowOrigin: rowOrigin, columnOrigin: columnOrigin, rowDestination: rowDestination, columnDestination: columnDestination)
        }
        
        let randomPlayerOne = RandomPlayer(withName: "Random Player 1", andId: .player1)
        let humanPlayerTwo = HumanPlayer(withName: "Random Player 2", andId: .player2, andInputMethod: input(_:))
        
        var rules = VerySimpleRules()
        var board_before = VerySimpleRules.createBoard()
        
        var move_chosen = randomPlayerOne!.chooseMove(in: board_before, with: rules)
        
        var board_after = playMove(board: board_before, move: move_chosen!)
        rules.playedMove(move: move_chosen!, boardBefore: board_before, boardAfter: board_after)
        
        while (!rules.isGameOver(board: board_after, lastCellRow: move_chosen!.rowDestination, lastCellColumn: move_chosen!.columnDestination).0) {
            let next_player_id = rules.getNextPlayer()
            var next_player: Player?
            switch next_player_id {
                case .player1:
                    next_player = randomPlayerOne
                    break
                case .player2:
                    next_player = humanPlayerTwo
                    break
                case .noOne:
                    print("an error occured")
            }
            board_before = board_after
            move_chosen = next_player?.chooseMove(in: board_before, with: rules)
            while (!rules.isMoveValid(board: board_before, move: move_chosen!)) {
                print("Move is not valid !")
                move_chosen = next_player?.chooseMove(in: board_before, with: rules)
            }
            board_after = playMove(board: board_before, move: move_chosen!)
            rules.playedMove(move: move_chosen!, boardBefore: board_before, boardAfter: board_after)
        }
        print("The game is over!")
    }
}
