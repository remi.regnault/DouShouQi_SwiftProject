//
//  BoardFailingReason.swift
//  
//
//  Created by Rémi REGNAULT on 16/01/2024.
//

import Foundation

public enum BoardFailingReason {
    case unknown, outOfBounds, cellNotEmpty, cellEmpty
}
