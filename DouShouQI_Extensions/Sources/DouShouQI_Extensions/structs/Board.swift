//
//  Cell.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation
import DouShouQI_Model

extension Board : CustomStringConvertible {
    public var description : String {
        var outputString = ""
        for row in grid {
            for cell in row {
                outputString += cell.cellType.symbol
                guard let currentPiece: Piece = cell.piece else {
                    outputString += "\t\t"
                    continue
                }
                outputString += currentPiece.animal.symbol
                outputString += currentPiece.owner.symbol
                outputString += "\t"
            }
            outputString += "\n"
        }
        return outputString
    }
}
