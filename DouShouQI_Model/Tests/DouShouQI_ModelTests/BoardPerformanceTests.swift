//
//  BoardPerformanceTests.swift
//  
//
//  Created by Rémi REGNAULT on 17/01/2024.
//

import XCTest
@testable import DouShouQI_Model

final class BoardPerformanceTests: XCTestCase {
    var initialBoard : Board!
    
    override func setUpWithError() throws {
        let grid = [
                      [
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .lion, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .den),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .tiger, withOwner: .player1))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .rat, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .leopard, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .wolf, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .elephant, withOwner: .player1))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .elephant, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .wolf, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .leopard, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .rat, withOwner: .player2))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .cat, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .dog, withOwner: .player2)),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .tiger, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .den),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .lion, withOwner: .player2))
                      ],
                  ]
        self.initialBoard = Board(withGrid: grid)
    }

    func testPerformanceCountPiecesOfOne() throws {
        self.measure {
            _ = initialBoard.countPieces(of: .player1)
        }
    }
    
    func testPerformanceCountPiecesOfAll() throws {
        self.measure {
            _ = initialBoard.countPieces()
        }
    }
    
    func testPerformanceInsertPiece() throws {
        self.measure {
            _ = initialBoard.insert(piece: Piece(andAnimal: .leopard, withOwner: .player1), atRow: 0, andColumn: 1)
        }
    }
    
    func testPerformanceRemovePiece() throws {
        self.measure {
            _ = initialBoard.removePiece(atRow: 0, andColumn: 0)
        }
    }

}
