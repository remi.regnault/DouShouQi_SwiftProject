//
//  BoardUnitTests.swift
//  
//
//  Created by Rémi REGNAULT on 17/01/2024.
//

import XCTest
import DouShouQI_Model

final class BoardUnitTests: XCTestCase {

    var initialBoard: Board!
    let nilBoard = Board(withGrid: 	[
        [
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .lion, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .den),
            Cell(ofType: .trap),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
            Cell(ofType: .jungle)
        ]
    ])
    let afterInsertionBoard = Board(withGrid: [
        [
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .lion, withOwner: .player1)),
            Cell(ofType: .jungle, withPiece: Piece(andAnimal: .leopard, withOwner: .player1)),
            Cell(ofType: .trap),
            Cell(ofType: .den),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .tiger, withOwner: .player1))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .rat, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .leopard, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .wolf, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .elephant, withOwner: .player1))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .elephant, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .wolf, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .leopard, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .rat, withOwner: .player2))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .cat, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .dog, withOwner: .player2)),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .tiger, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .den),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .lion, withOwner: .player2))
        ],
    ])
    let afterRemovalBoard = Board(withGrid: [
        [
            Cell(ofType: .jungle, ownedBy: .player1),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .den),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .tiger, withOwner: .player1))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .rat, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .leopard, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .wolf, withOwner: .player1)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .elephant, withOwner: .player1))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle),
            Cell(ofType: .water),
            Cell(ofType: .water),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .elephant, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .wolf, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .leopard, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .rat, withOwner: .player2))
        ],

        [
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .cat, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .dog, withOwner: .player2)),
            Cell(ofType: .jungle)
        ],

        [
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .tiger, withOwner: .player2)),
            Cell(ofType: .jungle),
            Cell(ofType: .trap),
            Cell(ofType: .den),
            Cell(ofType: .trap),
            Cell(ofType: .jungle),
            Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .lion, withOwner: .player2))
        ],
    ])
    
    override func setUpWithError() throws {
        let grid = [
                      [
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .lion, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .den),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .tiger, withOwner: .player1))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .rat, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .leopard, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .wolf, withOwner: .player1)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .elephant, withOwner: .player1))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle),
                          Cell(ofType: .water),
                          Cell(ofType: .water),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .elephant, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .wolf, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .leopard, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .rat, withOwner: .player2))
                      ],

                      [
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .cat, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .dog, withOwner: .player2)),
                          Cell(ofType: .jungle)
                      ],

                      [
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .tiger, withOwner: .player2)),
                          Cell(ofType: .jungle),
                          Cell(ofType: .trap),
                          Cell(ofType: .den),
                          Cell(ofType: .trap),
                          Cell(ofType: .jungle),
                          Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .lion, withOwner: .player2))
                      ],
                  ]
        self.initialBoard = Board(withGrid: grid)
    }
      
    func testsInitGrid() {
        XCTAssertNotNil(self.initialBoard)
        XCTAssertNil(self.nilBoard)
    }

    func testInsertion() {
        XCTAssertEqual(BoardResult.ok, self.initialBoard?.insert(piece: Piece(andAnimal: .leopard, withOwner: .player1), atRow: 0, andColumn: 1), "BoardResult is not .ok for a valid insertion")
        XCTAssertEqual(afterInsertionBoard, initialBoard, "The insertion function didn't correctly inserted the piece")
        XCTAssertEqual(BoardResult.failed(reason: .outOfBounds), self.initialBoard?.insert(piece: Piece(andAnimal: .leopard, withOwner: .player1), atRow: 49, andColumn: 1), "BoardResult is not .failed(.outofBounds) when trying to insert out of bounds")
        XCTAssertEqual(BoardResult.failed(reason: .cellNotEmpty), self.initialBoard?.insert(piece: Piece(andAnimal: .leopard, withOwner: .player1), atRow: 0, andColumn: 0), "BoardResult is not .failed(.cellNotEmpty) when inserting on a not empty cell")
    }

    func testRemoval() {
        XCTAssertEqual(BoardResult.ok, self.initialBoard?.removePiece(atRow: 0, andColumn: 0), "BoardResult is not .ok for a valid removal")
        XCTAssertEqual(afterRemovalBoard, initialBoard, "The removal function didn't correctly inserted the piece")
        XCTAssertEqual(BoardResult.failed(reason: .outOfBounds), self.initialBoard?.removePiece(atRow: 40, andColumn: 0), "BoardResult is not .failed(.outofBounds) when trying to delete out of bounds")
        XCTAssertEqual(BoardResult.failed(reason: .cellEmpty), self.initialBoard?.removePiece(atRow: 5, andColumn: 2), "BoardResult is not .failed(.cellEmpty) when removing on an empty cell")
    }

    func testCountPiecesOnePlayer() {
        XCTAssertEqual(8, self.initialBoard?.countPieces(of: .player1))
    }

    func testCountPiecesAllPlayers() {
        XCTAssertEqual(8, self.initialBoard?.countPieces().0)
        XCTAssertEqual(8, self.initialBoard?.countPieces().1)
    }
}
