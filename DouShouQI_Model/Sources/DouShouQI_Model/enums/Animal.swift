//
//  Animal.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public enum Animal: Comparable {
    case rat, cat, dog, wolf, leopard, tiger, lion, elephant
}
