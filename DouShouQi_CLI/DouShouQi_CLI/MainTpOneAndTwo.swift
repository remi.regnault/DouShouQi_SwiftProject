//
//  MainTpOneAndTwo.swift
//  DouShouQi_CLI
//
//  Created by Rémi REGNAULT on 05/02/2024.
//

import Foundation
import DouShouQI_Model
import DouShouQI_Extensions

struct MainTpOneAndTwo {
    static func main() throws {
        enum CustomErrors: Error {
            case isNil
        }
        let grid = [
            [
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .lion, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .trap),
                Cell(ofType: .den),
                Cell(ofType: .trap),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .tiger, withOwner: .player1))
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .dog, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .trap),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .cat, withOwner: .player1)),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .rat, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .leopard, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .wolf, withOwner: .player1)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player1, withPiece: Piece (andAnimal: .elephant, withOwner: .player1))
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle),
                Cell(ofType: .water),
                Cell(ofType: .water),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .elephant, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .wolf, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .leopard, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .rat, withOwner: .player2))
            ],
            
            [
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .cat, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .trap),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .dog, withOwner: .player2)),
                Cell(ofType: .jungle)
            ],
            
            [
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .tiger, withOwner: .player2)),
                Cell(ofType: .jungle),
                Cell(ofType: .trap),
                Cell(ofType: .den),
                Cell(ofType: .trap),
                Cell(ofType: .jungle),
                Cell(ofType: .jungle, ownedBy: .player2, withPiece: Piece (andAnimal: .lion, withOwner: .player2))
            ],
        ]
        let board = Board(withGrid: grid)
        guard var notnilboard : Board = board else {
            print("Board is nil!")
            throw(CustomErrors.isNil)
        }
        print("Initial Board :")
        print(notnilboard)
        print("Removing: \(notnilboard.removePiece(atRow: 0, andColumn: 0))")
        print("After Removal :")
        print(notnilboard)
        print("Inserting: \(notnilboard.insert(piece: Piece(andAnimal: .lion, withOwner: .player1), atRow: 0, andColumn: 1))")
        print("After Insertion :")
        print(notnilboard)
        print("countPiecesPlayer1 : \(notnilboard.countPieces(of: .player1))")
        print("countPiecesAllPlayers : \(notnilboard.countPieces())")
    }
}
