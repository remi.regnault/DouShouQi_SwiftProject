//
//  Rules.swift
//  
//
//  Created by Rémi REGNAULT on 22/01/2024.
//

import Foundation

public protocol Rules {
    var occurences: [Board: Int] { get }
    var historic: [Move] { get }

    static func createBoard() -> Board
    static func checkBoard(board: Board) throws
    func getNextPlayer() -> Owner
    func getMoves(board: Board, owner: Owner) -> [Move]
    func getMoves(board: Board, owner: Owner, row: Int, column: Int) -> [Move]
    func isMoveValid(board: Board, rowOrigin: Int, columnOrigin: Int, rowDestination: Int, columnDestination: Int) -> Bool
    func isMoveValid(board: Board, move: Move) -> Bool
    func isGameOver(board: Board, lastCellRow: Int, lastCellColumn: Int) -> (Bool, Result)
    mutating func playedMove(move: Move, boardBefore: Board, boardAfter: Board)
}
