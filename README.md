# DouShouQi_SwiftProject

## Introduction
Ce projet a été réalisé dans le cadre du cours *Swift IOS* de BUT 3ème année.

## Prérequis

* Sur MacOS
* Git
* Swift

## Utilisation

* Cloner le dépôt
* Ouvrir le projet
* Lancer le fichier *main.swift* dans le projet *DouShouQi.CLI*

## Résultat
### Sommaire
- [TP1](#tp1)
- [TP2](#tp2)
- [TP3](#tp3)
- [TP4](#tp4)
- [TP5](#tp5)
- [TP6](#tp6)

### TP1
#### Version à corriger
* Numéro du push à corriger : **55c1b18cfd759b6d42f166e1826d0b6a2d1bd971**
* Statut du rendu : **On time !**
* Pour tester : appeler la fonction tpOneAndTwo() dans le main.swift

#### Résumé
niveau | validé | description | coeff | pénalités TP2 | pénalités TP3 
--- | --- | --- | --- | --- | ---
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️ 
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️
🎬 | ✅ | les *Packages* et le test compilent | 4 | 50% | 75%
🎬 | ✅ | le test s'exécute sans bug | 4 | 50% | 75%
3️⃣ | ✅ | tous mes projets sont dans le même _workspace_ | 2 | 50% | 75%
1️⃣ | ✅ | j'ai créé un *Package* *Model* | 2 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```CellType``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```Owner``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```Animal``` | 1 | 50% | 75%
3️⃣ | ✅ | ```Owner``` s'affiche comme demandé sous la forme d'un ```String``` | 1 | 50% | 75%
1️⃣ | ✅ | ```Piece``` possède bien les propriétés demandées en lecture seule non-modifiables | 1 | 50% | 75%
1️⃣ | ✅ | ```Piece``` possède un initialiseur | 2 | 50% | 75%
3️⃣ | ✅ | ```Piece``` est transformable en ```String``` comme demandé | 1 | 50% | 75%
1️⃣ | ✅ | ```Cell``` possède bien les propriétés demandées en lecture seule non-modifiables | 1 | 50% | 75%
1️⃣ | ✅ | ```Cell``` possède bien une propriété ```piece``` pouvant ne pas avoir de valeur | 2 | 50% | 75%
1️⃣ | ✅ | ```Cell``` possède un initialiseur utilisant des valeurs par défaut pour deux paramètres | 2 | 50% | 75%
3️⃣ | ✅ | ```Cell``` est transformable en ```String``` comme demandé | 1 | 50% | 75%
1️⃣ | ✅ | ```Board``` possède bien les propriétés demandées en lecture seule non-modifiables | 1 | 50% | 75%
1️⃣ | ✅ | ```Board``` possède bien un tableau à deux dimensions de cellule | 2 | 50% | 75%
2️⃣ | ✅ | ```Board.grid``` est en lecture seule mais modifiable au sein de ```Board``` | 2 | 50% | 75%
1️⃣ | ✅ | ```Board``` possède un initialiseur permettant d'initialiser toutes ses propriétés | 2 | 50% | 75%
2️⃣ | ✅ | l'initialiseur de ```Board``` vérifie les contraintes et renvoie ```nil``` si nécessaire | 3 | 50% | 75%
3️⃣ | ✅ | j'ai créé un *Package* avec les extensions pour l'affichage en lignes de commande | 2 | 50% | 75%
3️⃣ | ✅ | j'ai créé une extensions pour ```CellType``` | 1 | 50% | 75%
3️⃣ | ✅ | j'ai créé une extensions pour ```Animal``` | 1 | 50% | 75%
3️⃣ | ✅ | j'ai créé une extensions pour ```Owner``` | 1 | 50% | 75%
3️⃣ | ✅ | j'ai créé une extensions pour ```Board``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai créé une application de type *Command Line Tool* utilisant les deux *Packages* précédents | 2 | 50% | 75%
2️⃣ | ✅ | j'initialise correctement ```Board``` dans le test | 2 | 50% | 75%
3️⃣ | ✅ | j'affiche correctement ```Board``` dans le test | 1 | 50% | 75%
3️⃣ | ✅ | mon dépôt possède un readme qui apporte quelque chose... | 2 | 50% | 75%
3️⃣ | | mon code est commenté | 1   | 50% | 75%

#### Affichage
L'affichage obtenu du plateau de jeu :
```
🌿🦁🟡	🌿	🪤	🪹	🪤	🌿	🌿🐯🟡	
🌿	🌿🐶🟡	🌿	🪤	🌿	🌿🐱🟡	🌿		
🌿🐭🟡	🌿	🌿🐆🟡	🌿	🌿🐺🟡	🌿	🌿🐘🟡	
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿🐘🔴	🌿	🌿🐺🔴	🌿	🌿🐆🔴	🌿	🌿🐭🔴	
🌿	🌿🐱🔴	🌿	🪤	🌿	🌿🐶🔴	🌿		
🌿🐯🔴	🌿	🪤	🪹	🪤	🌿	🌿🦁🔴	
```

### TP2
#### Version à corriger
* Numéro du push à corriger : **ead4b718565d038391321088504fb5a1620f9e00**
* Statut du rendu : **On time !**
* Pour tester : appeler la fonction tpOneAndTwo() dans le main.swift

#### Résumé
niveau | description | coeff | pénalités TP3 | pénalités TP4  
--- | --- | --- | --- | ---
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️ 
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️
🎬 |  | les *Packages* et le test compilent | 3 | 50% | 75%
🎬 |  | le test et les tests unitaires s'exécutent sans bug | 3 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```countPieces(of:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai utilisé des méthodes d'extension sur les collections | 3 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```countPieces()``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai utilisé un tuple nommé pour le retour | 2 | 50% | 75%
2️⃣ | ✅ | j'ai testé ces fonctions en CLI | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```BoardFailingReason``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```BoardResult``` | 3 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```insert(piece:atRow:andColumn:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai testé cette fonction en CLI | 1 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```removePiece(atRow:andColumn:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai testé cette fonction en CLI | 1 | 50% | 75%
1️⃣ | ✅ | je sais utiliser ```guard``` | 2 | 50% | 75%
2️⃣ | ✅ | je sais utiliser ```setUp``` ou ```setUpWithError``` | 4 | 50% | 75%
2️⃣ | ✅ | je sais utiliser un jeu de données pour un test unitaire | 5 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```countPieces(of:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```countPieces()``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```insert(piece:atRow:andColumn:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```removePiece(atRow:andColumn:)``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour l'initialiseur de ```Board``` | 2 | 50% | 75%
3️⃣ | ✅ | ma couverture de tests pour *Model* dépasse les 50% | 2 | 50% | 75%
3️⃣ | ✅ | ma couverture de tests pour *Model* dépasse les 70% | 3 | 50% | 75%
3️⃣ | ✅| ma couverture de tests pour *Model* dépasse les 85% | 3 | 50% | 75%
3️⃣ | ✅ | j'ai écrit les tests de performance | 3 | 50% | 75%
3️⃣ | | mon dépôt possède un readme qui apporte quelque chose... | 4 | 50% | 75%
3️⃣ | | mon code est commenté | 1   | 50% | 7

#### Affichage
```
Initial Board :
🌿🦁🟡	🌿	🪤	🪹	🪤	🌿	🌿🐯🟡	
🌿	🌿🐶🟡	🌿	🪤	🌿	🌿🐱🟡	🌿		
🌿🐭🟡	🌿	🌿🐆🟡	🌿	🌿🐺🟡	🌿	🌿🐘🟡	
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿🐘🔴	🌿	🌿🐺🔴	🌿	🌿🐆🔴	🌿	🌿🐭🔴	
🌿	🌿🐱🔴	🌿	🪤	🌿	🌿🐶🔴	🌿		
🌿🐯🔴	🌿	🪤	🪹	🪤	🌿	🌿🦁🔴	

Removing: ok
After Removal :
🌿	🌿	🪤	🪹	🪤	🌿	🌿🐯🟡	
🌿	🌿🐶🟡	🌿	🪤	🌿	🌿🐱🟡	🌿		
🌿🐭🟡	🌿	🌿🐆🟡	🌿	🌿🐺🟡	🌿	🌿🐘🟡	
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿🐘🔴	🌿	🌿🐺🔴	🌿	🌿🐆🔴	🌿	🌿🐭🔴	
🌿	🌿🐱🔴	🌿	🪤	🌿	🌿🐶🔴	🌿		
🌿🐯🔴	🌿	🪤	🪹	🪤	🌿	🌿🦁🔴	

Inserting: ok
After Insertion :
🌿	🌿🦁🟡	🪤	🪹	🪤	🌿	🌿🐯🟡	
🌿	🌿🐶🟡	🌿	🪤	🌿	🌿🐱🟡	🌿		
🌿🐭🟡	🌿	🌿🐆🟡	🌿	🌿🐺🟡	🌿	🌿🐘🟡	
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿	💧	💧	🌿	💧	💧	🌿		
🌿🐘🔴	🌿	🌿🐺🔴	🌿	🌿🐆🔴	🌿	🌿🐭🔴	
🌿	🌿🐱🔴	🌿	🪤	🌿	🌿🐶🔴	🌿		
🌿🐯🔴	🌿	🪤	🪹	🪤	🌿	🌿🦁🔴	

countPiecesPlayer1 : 8
countPiecesAllPlayers : (8, 8)
```

### TP3
#### Version à corriger
* Numéro du push à corriger : **adcc6dd42fe55fbf6a2ec9bddb0af092b122b97f**
* Statut du rendu : **On time !**

#### Nouvelle version à corriger
* Numéro du push à corriger : **6d0ec461eee961687ccc17ec61dfe84c39c3a69f**
* Statut du rendu : **A week late...**
* Correction : isMoveValid() ne fonctionnait pas correctement. Maintenant ça devrait être mieux.

#### Résumé
niveau | validé | description | coeff | pénalités TP4 | pénalités TP5  
--- | --- | --- | --- | --- | ---
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️ 
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️
🎬 | ✅ | les *Packages* et le test compilent | 3 | 50% | 75%
🎬 | ✅ | le test et les tests unitaires s'exécutent sans bug | 5 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```WinningReason``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```Result``` | 3 | 50% | 75%
1️⃣ | ✅ | j'ai créé la structure ```Move``` | 2 | 50% | 75%
3️⃣ | ✅ | j'ai ajouté une description à ```Move``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé le protocole ```Rules``` | 1 | 50% | 75%
3️⃣ || j'ai modifié ```Board``` à cause du protocole ```Rules``` | 2 | 50% | 75%
3️⃣ || j'ai testé ces modifications | 2 | 50% | 75%
1️⃣ | ✅ | j'ai ajouté le type ```VerySimpleRules``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```createBoard``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```InvalidBoardError``` | 2 | 50% | 75%
1️⃣ | ✅ | j'ai créé l'enum ```GameError``` | 2 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```checkBoard``` | 4 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```checkBoard``` | 3 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```getMoves``` (x2) | 3 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```getMoves``` | 2 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```isValidMove```, ```getNextPlayer``` | 6 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour  ```isValidMove```, ```getNextPlayer``` | 3 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```playedMove``` et géré l'historique | 1 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour  ```playedMove``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai écrit ```isGameOver``` | 6 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour  ```isGameOver``` | 3 | 50% | 75%
3️⃣ | ✅ | ma couverture de tests pour *Model* dépasse les 70% | 3 | 50% | 75%
3️⃣ | ✅ | ma couverture de tests pour *Model* dépasse les 90% | 3 | 50% | 75%
3️⃣ || mon dépôt possède un readme qui apporte quelque chose... | 3 | 50% | 75%
3️⃣ || mon code est commenté | 1   | 50% | 75%
🎉 || j'ai ajouté le type ```ClassicRules``` | - | 50% | 75%


### TP4
#### Version à corriger
* Numéro du push à corriger : **e78c901c3552fa405350b785a9ef3a0a1bd4408b**
* Statut du rendu : **On time !**
* Problème : la boucle de jeu ne fonctionne pas

#### Update
* Numéroe du push à corriger : **238c89580b1172d5d3e6f9a53fde2b192d7c2c93**
* Statut du rendu : **30's minutes late...**
* Ajout : Boucle de jeu RandomPlayer contre RandomPlayer fonctionnelle
* Pour tester : appeler la fonction gameplayTwoRandomPlayer() dans main.swift

#### Update
* Numéroe du push à corriger : **d93739fa1996d01d5eb8e7bbda99be720cf9d9e4**
* Statut du rendu : **1 hour late...**
* Ajout : Boucle de jeu RandomPlayer contre HumanPlayer fonctionnelle
* Pour tester : appeler la fonction gameplayOneRandomAndOneHuman() dans main.swift

#### Résumé
niveau | statut | description | coeff | pénalités TP5 | pénalités TP6  
--- | --- | --- | --- | --- | ---
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️ 
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️
🎬 | ✅ | les *Packages* et le test compilent | 1 | 50% | 75%
🎬 | ✅ | le test et les tests unitaires s'exécutent sans bug | 2 | 50% | 75%
1️⃣ | ✅ | j'ai créé la classe ```Player``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé la classe ```RandomPlayer``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé la classe ```HumanPlayer``` | 2 | 50% | 75%
1️⃣ | ✅ | j'ai ajouté l'injection de dépendance pour la saisie dans ```HumanPlayer``` | 4 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```RandomPlayer``` | 2 | 50% | 75%
2️⃣ | ✅ | j'ai écrit les tests unitaires pour ```HumanPlayer``` | 3 | 50% | 75%
1️⃣ | ✅ | j'ai testé dans une application en lignes de commande l'utilisation de ```RandomPlayer``` dans une boucle de jeu | 3 | 50% | 75%
1️⃣ | ✅ | j'ai testé dans une application en lignes de commande l'injection d'une méthode de saisie pour ```HumanPlayer``` | 2 | 50% | 75%
1️⃣ | ✅ | j'ai testé dans une application en lignes de commande l'utilisation de ```HumanPlayer``` dans une boucle de jeu | 2 | 50% | 75%
3️⃣ | ✅ | ma couverture de tests pour *Model* dépasse les 90% | 2 | 50% | 75%
3️⃣ || mon dépôt possède un readme qui apporte quelque chose... | 1 | 50% | 75%
3️⃣ || mon code est commenté | 1   | 50% | 75%
🎉 || j'ai ajouté une IA | - | 50% | 75%

### TP5
#### Version à corriger
* Numéro du push à corriger : **2aa9fd8bb7c63900ac18fe0ddebdd07583bbc642**
* Statut du rendu : **On time !**

#### Résumé

niveau | statut | description | coeff | pénalités TP6  
--- | --- | --- | --- | ---   
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️  
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️  
🎬 | ✅ | les *Packages* et le test compilent | 1 | 50%  
🎬 | ✅ | le test et les tests unitaires s'exécutent sans bug | 2 | 50%  
1️⃣ | ✅ | j'ai enrichi l'enum ```GameError``` | 1 | 50% | 75%
1️⃣ | ✅ | j'ai créé la structure ```Game```, ses membres et son initialiseur | 1 | 50%  
1️⃣ | ✅ | j'ai ajouté la fonction ```start()``` et géré la boucle de jeu | 6 | 50%  
1️⃣ | ✅ | j'ai ajouté la notification sur le démarrage du jeu | 1 | 50%  
1️⃣ | ✅ | j'ai abonné une méthode à la notification sur le démarrage du jeu | 1 | 50%  
1️⃣ | ✅ | j'ai testé la notification sur le démarrage du jeu | 1 | 50%  
1️⃣ | ✅ | j'ai ajouté la notification indiquant quel est le prochain joueur | 2 | 50%   
1️⃣ | ✅ | j'ai abonné une méthode à la notification indiquant quel est le prochain joueur | 2 | 50%  
1️⃣ | ✅ | j'ai testé la notification indiquant quel est le prochain joueur | 2 | 50%  
1️⃣ | ✅ | j'ai ajouté la notification donnant des informations sur la fin de partie (partie en cours, terminée, vainqueur...) | 4 | 50%  
1️⃣ | ✅ | j'ai abonné une méthode à la notification donnant des informations sur la fin de partie (partie en cours, terminée, vainqueur...) | 4 | 50%  
1️⃣ | ✅ | j'ai testé la notification donnant des informations sur la fin de partie (partie en cours, terminée, vainqueur...) | 4 | 50%  
2️⃣ | ✅ | j'ai ajouté la notification indiquant que le plateau de jeu a changé | 1 | 50%  
2️⃣ | ✅ | j'ai abonné une méthode à la notification indiquant que le plateau de jeu a changé | 1 | 50%  
2️⃣ | ✅ | j'ai testé la notification indiquant que le plateau de jeu a changé | 1 | 50%  
3️⃣ | ✅ | j'ai ajouté la notification indiquant quel coup a été choisi | 1 | 50%  
3️⃣ | ✅ | j'ai abonné une méthode à la notification indiquant quel coup a été choisi | 1 | 50%  
3️⃣ | ✅ | j'ai testé la notification indiquant quel coup a été choisi | 1 | 50%  
3️⃣ | ✅ | j'ai ajouté la notification indiquant que le coup choisi n'est pas valide | 1 | 50%  
3️⃣ | ✅ | j'ai abonné une méthode à la notification indiquant que le coup choisi n'est pas valide | 1 | 50%  
3️⃣ | ✅ | j'ai testé la notification indiquant que le coup choisi n'est pas valide | 1 | 50%  
2️⃣ | ✅ | mon test permet de lancer une partie entre deux joueurs aléatoires | 1   | 50%  
2️⃣ | ✅ | mon test permet de lancer une partie entre un joueur aléatoire et un joueur humain | 4   | 50%  
2️⃣ | ✅ | mon test permet de lancer une partie entre deux joueurs humains | 4   | 50%  
3️⃣ |  | mon dépôt possède un readme qui apporte quelque chose... | 2 | 50%  
3️⃣ |  | mon code est commenté | 1   | 50% 


### TP6
#### Version à corriger
* Numéro du push à corriger : **664513c9e258b3fd349388e99e1ab474e51ccab1**
* Statut du rendu : **On time !**
* What's new ?
 * Nouveau main: **MainTestsSaves**
 * Comment l'éxécuter ? Vérifier l'appel à la fonction **testsSaves** dans le fichier **main.swift**

#### Résumé

niveau | statut | description | coeff | pénalités   
--- | --- | --- | --- | ---    
☢️ | ✅ | Le dépôt doit être accessible par l'enseignant | ☢️ |  
☢️ | ✅ | un .gitignore doit exister au premier push | ☢️ |  
🎬 | ✅ | les *Packages* et le test compilent | 1 |     
🎬 | ✅ | le test et les tests unitaires s'exécutent sans bug | 4 |    
1️⃣ | ✅ | persistance de ```Animal```, ```Owner``` et ```CellType``` | |    
1️⃣ | ✅ | test de persistance de ```Animal```, ```Owner``` et ```CellType``` | 1 |   
1️⃣ | ✅ | persistance de ```Piece```, ```Move``` et ```Cell``` | |      
1️⃣ | ✅ | test de persistance de ```Piece```, ```Move``` et ```Cell``` | 1 |     
1️⃣ | ✅ | persistance de ```Board``` | |     
1️⃣ | ✅ | test de persistance de ```Board``` | 4 |    
1️⃣ | ✅ | persistance de ```Rules``` et les types conformes à ```Rules``` | |     
1️⃣ | ✅ | test de persistance de ```Rules``` et ```VerySimpleRules``` | 6 |    
1️⃣ | persistance de ```Player``` et ses types fils | |     
1️⃣ | test de persistance de ```Player```, ```RandomPlayer```, ```HumanPlayer``` | 6 |    
1️⃣ | persistance de ```Game``` | |     
2️⃣ | permettre de lancer une nouvelle partie et la dernière partie enregistrée | 12 |    
3️⃣ | ✅ | séparation persistance et modèle | 15 |     
3️⃣ | mon dépôt possède un readme qui apporte quelque chose... | 2 |    
3️⃣ | mon code est commenté | 1 |