//
//  Cell.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation

public struct Cell : CustomStringConvertible, Equatable, Hashable {
    public let cellType : CellType
    public let initialOwner : Owner
    public var piece : Piece?
    
    public init(ofType cellType : CellType, ownedBy initialOwner : Owner = .noOne, withPiece piece : Piece? = nil) {
        self.cellType = cellType
        self.initialOwner = initialOwner
        self.piece = piece
    }
    
    public var description : String {
        guard let localPiece: Piece = piece else {
            return "ø on \(cellType), \(initialOwner)"
        }
        return "\(localPiece) on \(cellType), \(initialOwner)"
    }
    
    public static func == (lhs: Cell, rhs: Cell) -> Bool {
        return lhs.cellType == rhs.cellType && lhs.initialOwner == rhs.initialOwner && lhs.piece == rhs.piece
    }
}
