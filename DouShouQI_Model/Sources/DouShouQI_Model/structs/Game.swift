//
//  Game.swift
//  
//
//  Created by Rémi REGNAULT on 05/02/2024.
//

import Foundation

public struct Game {
    public private(set) var board: Board
    public private(set) var rules: Rules
    public let playerOne: Player
    public let playerTwo: Player
    
    private var onStart: ((Board) -> ())?
    private var onBoardChanged: ((Board) -> ())?
    private var onPlayerTurn: ((Board, Player) -> ())?
    private var onInvalidPlayerMove: ((Player, Move?) -> ())?
    private var onPlayingMove: ((Move) -> ())?
    private var onGameIsOver: ((Result) -> ())?
    
    public init(withRules rules: Rules, andBoard board: Board, andPlayerOne playerOne: Player, andPlayerTwo playerTwo: Player) {
        self.board = board
        self.rules = rules
        self.playerOne = playerOne
        self.playerTwo = playerTwo
    }
    
    public mutating func setOnStart(onStartNotificationMethod onStart: @escaping (Board) -> ()) { self.onStart = onStart }
    public mutating func setOnBoardChanged(onBoardChangedNotificationMethod onBoardChanged: @escaping (Board) -> ()) { self.onBoardChanged = onBoardChanged }
    public mutating func setOnPlayerTurn(notifyPlayerTurnMethod onPlayerTurn: @escaping (Board, Player) -> ()) { self.onPlayerTurn = onPlayerTurn }
    public mutating func setOnInvalidPlayerMove(notifyInvalidPlayerMoveMethod onInvalidPlayerMove: @escaping (Player, Move?) -> ()) { self.onInvalidPlayerMove = onInvalidPlayerMove }
    public mutating func setOnPlayingMove(notifyOnPlayingMoveMethod onPlayingMove: @escaping (Move) -> ()) { self.onPlayingMove = onPlayingMove }
    public mutating func setOnGameIsOver(onGameIsOverNotificationMethod onGameIsOver: @escaping (Result) -> ()) { self.onGameIsOver = onGameIsOver }
       
    public mutating func start() throws {
        if let onStart: (Board) -> () = onStart {
            onStart(board)
        }
        
        var currentMove = try playTurn()
        var (isGameOver, gameInfos) = rules.isGameOver(board: board, lastCellRow: currentMove.rowDestination, lastCellColumn: currentMove.columnDestination)
        while (!isGameOver) {
            currentMove = try playTurn()
            (isGameOver, gameInfos) = rules.isGameOver(board: board, lastCellRow: currentMove.rowDestination, lastCellColumn: currentMove.columnDestination)
            
            if let onGameIsOver: (Result) -> () {
                onGameIsOver(gameInfos)
            }
        }
    }
    
    private func askForValidPlayerMove(_ player: Player) throws -> Move {
        var selectedMove: Move? = player.chooseMove(in: self.board, with: self.rules)
        while (selectedMove == nil || !rules.isMoveValid(board: self.board, move: selectedMove!)) {
            if let onInvalidPlayerMove: (Player, Move?) -> () = onInvalidPlayerMove {
                onInvalidPlayerMove(player, selectedMove)
            }
            selectedMove = player.chooseMove(in: self.board, with: self.rules)
        }
        return selectedMove!
    }
    
    private func getNextPlayer() throws -> Player {
        let nextOwner = self.rules.getNextPlayer()
        switch (nextOwner) {
            case .player1:
                return self.playerOne
            case .player2:
                return self.playerTwo
            case .noOne:
                throw GameError.nextPlayerError
        }
    }
    
    private mutating func playTurn() throws -> Move {
        let nextPlayer = try self.getNextPlayer()
        if let onPlayerTurn: (Board, Player) -> () = onPlayerTurn {
            onPlayerTurn(board, nextPlayer)
        }
        
        let nextMove: Move = try askForValidPlayerMove(nextPlayer)
        if let onPlayingMove: (Move) -> () = onPlayingMove {
            onPlayingMove(nextMove)
        }
        playMove(nextMove)
        return nextMove
    }
    
    private mutating func playMove(_ moveToPlay: Move) {
        let boardBefore = board
        if board.grid[moveToPlay.rowDestination][moveToPlay.columnDestination].piece != nil {
            _ = board.removePiece(atRow: moveToPlay.rowDestination, andColumn: moveToPlay.columnDestination)
        }
        _ = board.insert(piece: board.grid[moveToPlay.rowOrigin][moveToPlay.columnOrigin].piece!, atRow: moveToPlay.rowDestination, andColumn: moveToPlay.columnDestination)
        _ = board.removePiece(atRow: moveToPlay.rowOrigin, andColumn: moveToPlay.columnOrigin)
        rules.playedMove(move: moveToPlay, boardBefore: boardBefore, boardAfter: board)
        
        if let onBoardChanged: (Board) -> () = onBoardChanged {
            onBoardChanged(board)
        }
    }
}
