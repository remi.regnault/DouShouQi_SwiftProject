//
//  Owner.swift
//  
//
//  Created by Rémi REGNAULT on 12/01/2024.
//

import Foundation
import DouShouQI_Model

extension Owner {
    public var symbol: String {
        switch self {
            case .noOne:
                return " "
            case .player1:
                return "🟡"
            case .player2:
                return "🔴"
        }
    }
}
