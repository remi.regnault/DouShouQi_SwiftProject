import XCTest
@testable import DouShouQI_Saver

final class DouShouQI_SaverTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DouShouQI_Saver().text, "Hello, World!")
    }
}
