//
//  Player.swift
//  
//
//  Created by Rémi REGNAULT on 29/01/2024.
//

import Foundation

public class Player {
    public let id: Owner
    public let name: String
    
    public init?(withName name: String, andId id: Owner) {
        if (id == .noOne) { return nil }
        if (name.isEmpty) { return nil }
        self.name = name
        self.id = id
    }
    
    public func chooseMove(in board: Board, with rules: Rules) -> Move? { return nil }
}
