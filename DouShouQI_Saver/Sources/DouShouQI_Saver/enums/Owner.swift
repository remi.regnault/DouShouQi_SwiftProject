//
//  Owner.swift
//  
//
//  Created by Rémi REGNAULT on 12/02/2024.
//

import Foundation
import DouShouQI_Model

extension Owner: Codable {
    enum Key: CodingKey {
        case value
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let value = try container.decode(String.self, forKey: .value)
        switch value {
            case "noOne":
                self = .noOne
            case "playerOne":
                self = .player1
            case "playerTwo":
                self = .player2
            default:
                throw LoadingError.unknownValue(value: value)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
            switch self {
            case .noOne:
                try container.encode("noOne", forKey: .value)
            case .player1:
                try container.encode("playerOne", forKey: .value)
            case .player2:
                try container.encode("playerTwo", forKey: .value)
        }
    }
}
